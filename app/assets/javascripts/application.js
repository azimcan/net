// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .

//= require jquery3
//= require popper
//= require bootstrap

function input(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var input = document.createElement("input");
	input.setAttribute("id", params.id);
	input.setAttribute("name", params.name);
	input.setAttribute("type", params.type);
	return input;
}

function td(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var td = document.createElement("td");
	params.text && td.appendChild( document.createTextNode(params.text) );
	params.child && td.appendChild(params.child);
	return td;
}
function th(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var th = document.createElement("th");
	params.text && button.appendChild( document.createTextNode(params.text) );
	params.scope && th.setAttribute("scope", params.scope);
	return th;
}

function tr(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var tr = document.createElement("tr");
	params.id && tr.setAttribute("id", params.id)
	return tr;
}

function button(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var button = document.createElement("button");
	params.text && button.appendChild( document.createTextNode(params.text) );
	params.class_ && button.setAttribute("class", params.class_);
	params.type && button.setAttribute("type", params.type);
	return button;
}

function div(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var div = document.createElement("div");
	params.child && div.appendChild( params.child );
	params.class_ && div.setAttribute("class", params.class_);
	return div;
}

function canvas(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var canvas = document.createElement("canvas");
	params.child && canvas.appendChild( params.child );
	params.class_ && canvas.setAttribute("class", params.class_);
	params.style && canvas.setAttribute("style", params.style);
	return canvas;
}

function table(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var table = document.createElement("table");
	params.child && table.appendChild( params.child );
	params.class_ && table.setAttribute("class", params.class_);
	return table;
}

function thead(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var thead = document.createElement("thead");
	params.child && thead.appendChild( params.child );
	params.class_ && thead.setAttribute("class", params.class_);
	params.style && thead.setAttribute("style", params.style);
	return thead;
}

function tbody(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var tbody = document.createElement("tbody");
	params.child && tbody.appendChild( params.child );
	params.class_ && tbody.setAttribute("class", params.class_);
	return tbody;
}