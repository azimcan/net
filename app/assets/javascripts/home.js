function input(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var input = document.createElement("input");
	input.setAttribute("id", params.id);
	input.setAttribute("name", params.name);
	input.setAttribute("type", params.type);
	return input;
}

function td(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var td = document.createElement("td");
	params.child && td.appendChild(params.child);
	return td;
}
function th(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var th = document.createElement("th");
	var text = params.text && document.createTextNode(params.text);
	params.text && th.appendChild(text);
	params.scope && th.setAttribute("scope", params.scope);
	return th;
}

function tr(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var tr = document.createElement("tr");
	params.id && tr.setAttribute("id", params.id)
	return tr;
}

function button(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var button = document.createElement("button");
	params.text && button.appendChild( document.createTextNode(params.text) );
	params.class_ && button.setAttribute("class", params.class_);
	params.type && button.setAttribute("type", params.type);
	return button;
}

function div(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null}){
	var div = document.createElement("div");
	params.child && div.appendChild( params.child );
	params.class_ && div.setAttribute("class", params.class_);
	return div;
}

function canvas(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var canvas = document.createElement("canvas");
	params.child && canvas.appendChild( params.child );
	params.class_ && canvas.setAttribute("class", params.class_);
	params.style && canvas.setAttribute("style", params.style);
	return canvas;
}

function table(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var table = document.createElement("table");
	params.child && table.appendChild( params.child );
	params.class_ && table.setAttribute("class", params.class_);
	return table;
}

function thead(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var thead = document.createElement("thead");
	params.child && thead.appendChild( params.child );
	params.class_ && thead.setAttribute("class", params.class_);
	params.style && thead.setAttribute("style", params.style);
	return thead;
}

function tbody(params = {class_:null, id:null, type:null, name:null, text:null, child:null, scope:null, style:null}){
	var tbody = document.createElement("tbody");
	params.child && tbody.appendChild( params.child );
	params.class_ && tbody.setAttribute("class", params.class_);
	return tbody;
}